

--[[
Copyright © 2012 Jorge Ortiz

    This file is part of StripGrid.

    StripGrid is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    StripGrid is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with StripGrid.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- Use this function to perform your initial setup
function setup()
    saveProjectInfo("Author", "Jorge Ortiz")
    --displayMode(FULLSCREEN)
    iparameter("Debug",0,1)
    debug = false
    orientationChanged(CurrentOrientation) 
end

function orientationChanged(newOrientation) 

    listener = SampleListener() -- Listens to cell selection events
    
    if newOrientation == PORTRAIT 
            or newOrientation == PORTRAIT_UPSIDE_DOWN then
        grid = Grid(
            Strip.VERT, 
            50, 50, -- Grid's left bottom corner coords.
            WIDTH - 100, HEIGHT - 100, -- Grid's width and height
            2, 4, -- number of rows and colums
            listener
        )
    elseif newOrientation == LANDSCAPE_LEFT 
            or newOrientation == LANDSCAPE_RIGHT then
        grid = Grid(
            Strip.HORIZ,
            50, 50, -- Grid's left bottom corner coords.
            WIDTH - 100, HEIGHT - 100, -- Grid's width and height
            2, 2, -- number of rows and colums
            listener
        )
        orientation = Strip.HORIZ
    end

    grid:setDebug(debug)
    --grid:addSampleCells(105)
    
    --cell = grid:add(SampleContent)
    --print("Sample content added to cell "..cell.index)
    
    for _,imgName in ipairs(spriteList("Documents")) do
        --print(imgName)
        cell = grid:add(Picture("Documents", imgName, 50))
        cell.firstShowListener = listener
    end
end

function draw()
    background(179, 179, 179, 255)
    if debug then
        textMode(CENTER)
        fontSize(100)
        font("ArialMT")
        text(WIDTH.." x "..HEIGHT, WIDTH/2, HEIGHT/2)
    end
    if grid then
        grid:draw()
    end
    -- Move randomly to show that it can be moved
    if math.random(200) == 1 then
        local newX = grid.x + math.random(-50,50)
        local newY = grid.y + math.random(-50,50)
        newX = math.max(0, math.min(100, newX))
        newY = math.max(0, math.min(100, newY))
        grid:moveTo(newX, newY)
    end
    if selectedImage then
        grid.enabled = false
        spriteMode(CORNER)
        sprite(selectedImage, 0, 0, WIDTH, HEIGHT)
    end
end

function touched(touch)
    if grid then
        grid:touched(touch)
    end
    newDebug = Debug == 1
    if newDebug ~= debug then
        debug = newDebug
        -- Force recreation of Grid with new debug mode...
        orientationChanged(CurrentOrientation)
    end
end
