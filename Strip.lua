--[[
Copyright © 2012 Jorge Ortiz

    This file is part of StripGrid.

    StripGrid is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    StripGrid is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with StripGrid.  If not, see <http://www.gnu.org/licenses/>.
--]]

Strip = class()

Strip.HORIZ = 1
Strip.VERT = 2

function Strip:init(orientation, min, max, shift, onSwipe, listener)
    self.orientation = orientation
    self:setMin(min)
    self:setMax(max)
    self:setShift(shift)
    self.onSwipe = onSwipe
    self.listener = listener
    
    self.debug = false
    
    self.speed = 0
    self.deceleration = 1.05
    
    if orientation == Strip.HORIZ then
        self.calculateSpeed = self.calculateHorizSpeed
        self.translateTouch = self.translateHorizTouch
    else
        self.calculateSpeed = self.calculateVertSpeed
        self.translateTouch = self.translateVertTouch
    end
end

function Strip:setMin(min)
    if min then
        self.min = min
    else
        self.min = 0
    end
end

function Strip:setMax(max)
    if max then
        self.max = max
    else
        self.max = WIDTH
    end
end

function Strip:setShift(shift)
    if shift then
        self.shift = shift
    else
        self.shift = 0
    end
end

function Strip:draw()
    if self.debug then
        pushStyle()
        fill(0, 0, 0, 255)
        font("Arial-BoldMT")
        fontSize(20)
        textMode(CENTER)
        for i = 0, 80 do
            if self.orientation == Strip.HORIZ then
                text(i*100 - 4000, -4000 + (100 * i) - self.shift, HEIGHT/2)
            else
                text(i*100 - 4000, WIDTH/2, 4000 - (100 * i) + self.shift)
            end
        end
        popStyle()
    end
    self:move()
end

function Strip:touched(touch)
    --if touch.state == ENDED then
        self.translateTouch(self, touch)
    --else
    if touch.state == MOVING then
        self.calculateSpeed(self, touch)
    elseif touch.state == BEGAN then
        self.speed = 0
    end
end

function Strip:move()
    if math.abs(self.speed) > 1 then
        self:doMove()
        self.speed = self.speed / self.deceleration
        if math.abs(self.speed) < 1 then
            self.speed = 0
        end
    end
end
            
function Strip:doMove()
    self.shift = self.shift + math.floor(self.speed)
    if self.shift > self.max then
        self.shift = self.max
        self.speed = 0
        sound(SOUND_HIT, 22326)
    elseif self.shift < self.min then
        self.shift = self.min
        self.speed = 0
        sound(SOUND_HIT, 22326)
    end
    if self.onSwipe then
        self:onSwipe(self.listener, self.shift)
    end
end

function Strip:calculateHorizSpeed(touch)
    self.speed = touch.prevX - touch.x
end

function Strip:calculateVertSpeed(touch)
    self.speed = touch.y - touch.prevY
end

function Strip:translateHorizTouch(touch)
    self.touchPoint = vec2(touch.x + self.shift, touch.y)
    return self.touchPoint
end

function Strip:translateVertTouch(touch)
    self.touchPoint = vec2(touch.x, self.shift - touch.y)
end

function Strip:moving()
    return self.speed ~= 0
end


