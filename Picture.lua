--[[
Copyright © 2012 Jorge Ortiz

    This file is part of StripGrid.

    StripGrid is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    StripGrid is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with StripGrid.  If not, see <http://www.gnu.org/licenses/>.
--]]

Picture = class()

function Picture:init(collection, fileName, margin)
    self.collection = collection
    self.fileName = fileName
    self.path = collection..":"..fileName
    if margin then
        self.margin = margin
    else
        self.margin = 0
    end
end

function Picture:draw(cell)
    spriteMode(CORNER)
    if cell then
        sprite(self.path, cell.x + self.margin, 
            cell.y + self.margin, 
            cell.w - self.margin * 2, 
            cell.h - self.margin * 2)
    else
        sprite(self.path)
    end
end
