--[[
Copyright © 2012 Jorge Ortiz

    This file is part of StripGrid.

    StripGrid is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    StripGrid is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with StripGrid.  If not, see <http://www.gnu.org/licenses/>.
--]]

Grid = class()

function Grid:init(orientation, x, y, w, h, 
                        lanes, visibleCellsPerLane, listener)
    self.orientation = orientation
    self.x = x
    self.y = y
    self.w = w
    self.h = h
    self.lanes = lanes

    self.listener = listener
    
    self.enabled = true
    self.debug = false
    
    if orientation == Strip.HORIZ then
        self.laneWidth = math.floor(h / lanes)
        self.cellLength = math.floor(w / visibleCellsPerLane)
        self.shiftCells = self.shiftCellsHorizontally
        self.getCell = self.getCellHoriz
    else
        self.laneWidth = math.floor(w / lanes)
        self.cellLength = math.floor(h / visibleCellsPerLane)
        self.shiftCells = self.shiftCellsVertically
        self.getCell = self.getCellVert
    end
    
    --self.width = self.laneWidth * lanes
    self.strip  = Strip(orientation, 0, 0, 0, self.onSwipe, self)
    self.strip.debug = self.debug
    
    self.cells = {}
end

function Grid:setDebug(debug)
    self.debug = debug
    self.strip.debug = debug
    for _,cell in ipairs(self.cells) do
        cell.debug = debug
    end
end

function Grid:draw()
    if self.enabled then
        self:frame()
        clip(self.x, self.y, self.w, self.h)
        self.strip:draw()
        for _,cell in ipairs(self.cells) do
            cell:draw()
        end
        clip()
    end
end

function Grid:frame()
    rectMode(CORNER)
    fill(255, 255, 255, 255)
    noStroke()
    rect(self.x, self.y, self.w, self.h)
    
    local w = 20
    strokeWidth(w)
    stroke(0, 0, 0, 100)
    noFill()
    rect(self.x - w, self.y - w, self.w + w*2, self.h + w*2)
end

function Grid:moveTo(newX, newY)
    self.x = newX
    self.y = newY
    for index,cell in ipairs(self.cells) do
        local x, y = self:getCoord(index)
        --if self.orientation == Strip.HORIZ then
        cell.x = x
        cell.y = y
        cell.origX = x
        cell.origY = y
        --cell:isVisible(self)
    end
    self:shiftCells(self.strip.shift)
end

function Grid:onSwipe(listener, shift)
    listener:shiftCells(shift)
    listener:calculateVisibleCells()
end

function Grid:shiftCellsHorizontally(shift)
    for _,cell in ipairs(self.cells) do
        cell.x = cell.origX - shift
    end
end

function Grid:shiftCellsVertically(shift)
    for _,cell in ipairs(self.cells) do
        cell.y = cell.origY + shift
    end
end

function Grid:touched(touch)
    if self:contains(touch.x, touch.y) then
        self.strip:touched(touch)

        if touch.state == BEGAN then
            self.cellIndex = self:getCellIndex(self.strip.touchPoint)
        elseif touch.state == ENDED then
            local cellIndex = self:getCellIndex(self.strip.touchPoint)
            if cellIndex == self.cellIndex then
                if self.cellIndex > 0 then
                    self.cells[cellIndex]:touched(touch)
                end
            end
        end
    end
end

function Grid:contains(x, y)
    return self.x < x and x < (self.x + self.w) 
        and self.y < y and y < (self.y + self.h)
end

function Grid:calculateVisibleCells()
    for _,cell in ipairs(self.cells) do
        cell:isVisible(self)
    end
end

function Grid:add(content)
    local index = #self.cells + 1
    --print("index "..index)
    local x, y = self:getCoord(index)
    --print(x, y)
    
    local cell = nil
    if self.orientation == Strip.HORIZ then
        cell = Cell(index, x, y, self.cellLength, self.laneWidth, content,
            self.onDoubleTap, self.listener)
    else
        cell = Cell(index, x, y, self.laneWidth, self.cellLength, content,
            self.onDoubleTap, self.listener)
    end
    cell:isVisible(self)
    cell.debug = self.debug
    table.insert(self.cells, cell)
    --cell.show = self.showCenters
    self:adjustStripLength()
    return cell
end

-- This function is for debugging purposes only
function Grid:showCenters(cell)
    pushStyle()
    stroke(255, 0, 0, 255)
    strokeWidth(1)
    noSmooth()
    line(0,0,cell.x + cell.w/2, cell.y + cell.h/2)
    cell:defaultShow(cell)
    popStyle()
end

function Grid:adjustStripLength()
    local _,column = self:getPosition(#self.cells)
    if self.orientation == Strip.HORIZ then
        self.strip.max = column * self.cellLength - self.w
    else
        self.strip.max = column * self.cellLength - self.h
    end
end

function Grid:getPosition(index)
    local column,_ = math.modf((index - 1) / self.lanes) + 1
    local lane = math.fmod(index - 1, self.lanes) + 1
    return lane, column
end

function Grid:getCoord(index)
    local lane, column = self:getPosition(index)
    --print("lane "..lane, "column "..column)
    local x, y
    if self.orientation == Strip.HORIZ then
        x = self.x + (column - 1) * self.cellLength
        y = self.y + self.h - lane * self.laneWidth
    else
        x = self.x + (lane - 1) * self.laneWidth
        y = self.y - column * self.cellLength + self.h
        --return self.x + self.w - y, self.y + self.h - x
    end
    return x, y
end

function Grid:getCellHoriz(coord)
    local column = math.floor((coord.x - self.x) / self.cellLength) + 1
    local lane = math.floor((self.y + self.h - coord.y) / self.laneWidth) + 1
    --print(coord.x..","..coord.y..": "..lane..","..column)
    return lane, column
end

function Grid:getCellVert(coord)
    local column = math.floor((self.y + self.h + coord.y) / self.cellLength) + 1
    local lane = math.floor((coord.x - self.x) / self.laneWidth) + 1
    --print(coord.x..","..coord.y..": "..lane..","..column)
    return lane, column
end

function Grid:getCellIndex(coord)
    local lane, column = self:getCell(coord)
    local index = (column - 1) * self.lanes + lane
    if index > #self.cells then
        index = 0
    end
    --print("lane:"..lane..",column:"..column..",cell:"..index)
    return index
end

-- This function is for testing purposes only
function Grid:addSampleCells(count)
    for i = 1,count do
       self:add(SampleContent())
    end
end
