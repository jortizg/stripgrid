--[[
Copyright © 2012 Jorge Ortiz

    This file is part of StripGrid.

    StripGrid is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    StripGrid is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with StripGrid.  If not, see <http://www.gnu.org/licenses/>.
--]]

Cell = class()

function Cell:init(index, x, y, w, h, content, listener)
    --print(index, x, y, h, content)
    
    self.index = index
    self.origX = x
    self.origY = y
    self.x = x
    self.y = y
    self.w = w
    self.h = h
    self.content = content
    self.listener = listener
    
    self.visible = false
    self.show = Cell.defaultShow
    self.shownBefore = false
end

function Cell:defaultShow(pos)
    pushStyle()
        
    fill(223, 203, 141, 100)
    rectMode(CORNER)
    strokeWidth(3)
    stroke(255, 0, 0, 100)
    rect(self.x, self.y, self.w, self.h)
        
    font("AmericanTypewriter-Bold")
    fontSize(50)
    fill(0, 0, 255, 100)
    textMode(CENTER)
    text(self.index, self.x + (self.w / 2), self.y + (self.h / 2))
        
    popStyle()
end

function Cell:draw()
    if self.visible then
        pushStyle()
        if self.debug and self.show then
            self:show(self)
        end
        if self.content and self.content.draw then
            self.content:draw(self)
        end
        if self.selected then
            self:highlight()
        end
        if self.debug then
            self:showCoords()
        end
        popStyle()
    end
end

function Cell:isVisible(window)
    self.visible = (((self.x + self.w) >= window.x) 
                and (self.x < (window.x + window.w)))
                and (self.y < (window.y + window.h)) 
                and ((self.y + self.h) >= window.y)
    if self.visible and (not self.shownBefore) then
        self.shownBefore = true
        if self.firstShowListener and self.firstShowListener.onFirstShow then
            self.firstShowListener:onFirstShow(self.firstShowListener, self)
        end
    end
    return self.visible
end

function Cell:highlight()
    fill(0, 0, 255, 100)
    stroke(0, 0, 255, 255)
    strokeWidth(10)
    rectMode(CORNER)
    rect(self.x, self.y, self.w, self.h)
end

function Cell:showCoords()
    font("ArialMT")
    fontSize(15)
    fill(0, 0, 0, 255)
    textMode(CORNER)
    text(self.x..","..self.y, self.x + 5, self.y + 2)
    text(self.w.." x "..self.h, self.x + 5, self.y + self.h - 22)
end

function Cell:touched(touch)
    if touch.tapCount == 1 then
        self.selected = not self.selected
        if listener.onSingleTap then
            listener:onSingleTap(listener, self)
        end
    elseif (touch.tapCount == 2) and listener.onDoubleTap then
        listener:onDoubleTap(listener, self)
    end
    if self.content and self.content.touched then
        self.content:touched(touch, self)
    end
end
