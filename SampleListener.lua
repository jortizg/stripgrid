--[[
Copyright © 2012 Jorge Ortiz

    This file is part of StripGrid.

    StripGrid is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    StripGrid is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with StripGrid.  If not, see <http://www.gnu.org/licenses/>.
--]]

SampleListener = class()

function SampleListener:init()
end

function SampleListener:onSingleTap(listener, cell)
    if debug then
        print("cell["..cell.index.."] "..cell.content.path.." (1)")
    end
end

function SampleListener:onDoubleTap(listener, cell)
    if debug then
        print("cell["..cell.index.."] "..cell.content.path.." (2)")
    end
    -- This global variable can be used in Main to show the selected image
    selectedImage = cell.content.path
end

function SampleListener:onFirstShow(listener, cell)
    if debug then
        print("first time cell["..cell.index.."] is shown")
    end
end