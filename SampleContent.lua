--[[
Copyright © 2012 Jorge Ortiz

    This file is part of StripGrid.

    StripGrid is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    StripGrid is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with StripGrid.  If not, see <http://www.gnu.org/licenses/>.
--]]

SampleContent = class()

function SampleContent:init()
    self.id = math.random()
end

function SampleContent:draw(cell)
    ellipseMode(CENTER)
    fill(0, 255, 0, 50)
    ellipse(cell.x + cell.w/2, cell.y + cell.h/2, 100)
end

function SampleContent:touched(touch, cell)
    print("Content with id ["..self.id.."] in cell["
        ..cell.index.."] was touched with touch["..touch.id.."]")
end

function SampleContent:onDoubleTap(cell)
    grid.enabled = false
    print(cell.index)
end